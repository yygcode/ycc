#include <stdio.h>
#include <stdlib.h>

static int cmp(const void *p1, const void *p2)
{
	return *(int*)p1 - *(int*)p2;
}

#define arr_pr(arr)	pr(arr, sizeof(arr)/sizeof(arr[0]))
static void pr(const int *v, size_t n)
{
	size_t i;

	printf("arr(n=%zu): ", n);
	for (i = 0; i < n; ++i)
		printf("%-3d ", v[i]);
	printf("\n");
}

int main(int argc, char **argv)
{
	int arr[] = {1, 5, 100, 20, 78, 92, 3, 62, 74};

	arr_pr(arr);
	qsort(arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), cmp);
	arr_pr(arr);

	return 0;
}
