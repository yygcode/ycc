#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>

#include <ycc/algos/string.h>

static char *native_find(const char *haystack, const char *needle, size_t n)
{
	size_t i;

	while (*haystack) {
		for (i = 0; i < n; ++i) {
			if (haystack[i] != needle[i])
				break;
		}

		if (i == n)
			return (char*)haystack;

		++haystack;
	}

	return NULL;
}

int correct_verify(void)
{
#undef NL_LEN
#define NL_LEN	32
	const char haystack[NL_LEN*2] =
		"I'm yanyg, i'm from china, yyg is a good boy~!";
	char patn[NL_LEN], *result;
	size_t table_sgs[NL_LEN*2], table_ebc[UCHAR_MAX+1];
	size_t tlen = strlen(haystack), nl;

	int i;
	for (i = 0; i < tlen; ++i) {
		for (nl = 0; nl < NL_LEN && nl < tlen - i; ++nl) {
			strncpy(patn, haystack+i, nl);
			patn[nl] = '\0';

			strkmp_init(patn, table_sgs);
			result = strkmp_find(haystack, patn, table_sgs);
			if (!result) {
				fprintf(stderr, "kmp find failed !\n");
				return EINVAL;
			}

			strbmh_init(patn, nl, table_ebc);
			result = strbmh_find(haystack, patn, tlen, nl,
					     table_ebc);
			if (!result) {
				fprintf(stderr, "bmh find failed !\n");
				return EINVAL;
			}

			strbm_init(patn, nl, table_sgs, table_ebc);
			result = strbm_find(haystack, patn, tlen, nl,
					    table_sgs, table_ebc);
			if (!result) {
				fprintf(stderr, "bm find failed !\n");
				return EINVAL;
			}
		}
	}

	return 0;
}

static unsigned long calc_microsec(const struct timeval *tb, const char *str)
{
	struct timeval te;
	unsigned long x;

	gettimeofday(&te, NULL);

	if (te.tv_usec < tb->tv_usec) {
		te.tv_usec += 1000*1000;
		--te.tv_sec;
	}

	x = (unsigned long)((te.tv_sec - tb->tv_sec)*1000*1000 +
			    (te.tv_usec - tb->tv_usec));
	printf("\t\tstr(%6s) microsec(%lu)\n", str, x);

	return x;
}

struct perf_info {
	unsigned long microsec;
	const char *desc;
};

static int picmp(const void *x1, const void *x2)
{
	return (int)((struct perf_info *)x1)->microsec -
	       (int)((struct perf_info *)x2)->microsec;
}

static void do_sort(unsigned offset, const unsigned long *pn, const char **desc,
		    int n)
{
	int i;
	struct perf_info *spn = malloc(sizeof(*spn)*n);

	if (!spn) {
		/* ignore sort */
		fprintf(stderr, "do_sort could not alloc buffer, ignore\n");
		return;
	}

	for (i = 0; i < n; ++i) {
		spn[i].microsec = pn[i];
		spn[i].desc = desc[i];
	}

	qsort(spn, n,  sizeof(*spn), picmp);

	printf("offset(%u) sort: ", offset);
	for (i = 0; i < n; ++i) {
		printf("%s(%lu) ", spn[i].desc, spn[i].microsec);
	}
	printf("\n\n");
}

static int perf_verify(void)
{
#undef TLEN
#define TLEN 1024*1024*10

#undef PTLEN
#define PTLEN	10240

#undef REPEAT
#define REPEAT 100000

#undef NSF
#define NSF	5
	char *haystack, patn[PTLEN+1], *result;
	size_t *table_sgs, table_ebc[UCHAR_MAX+1];
	struct timeval tb;
	unsigned long microsec[NSF];
	const char *desc[NSF] = {"strstr", "native", "kmp", "bmh", "bm"};
	unsigned int i, j;

	haystack = malloc(TLEN);
	if (!haystack) {
		fprintf(stderr, "perf_verify alloc haystack failed !\n");
		return ENOMEM;
	}

	table_sgs = malloc(TLEN*2);
	if (!haystack) {
		fprintf(stderr, "perf_verify alloc table failed !\n");
		free(haystack);
		return ENOMEM;
	}

	srand(time(NULL));
	for (i = 0; i < TLEN; ++i) {
		haystack[i] = (char)(rand()%255) + 1;
		if (!haystack[i]) {
			fprintf(stderr, "wrong val !!! i = %u\n", i);
			return EINVAL;
		}
	}
	printf("tlen=%zu\n", strlen(haystack));

	for (i = 0; i < TLEN - PTLEN; i += PTLEN*10) {
		strncpy(patn, haystack + i, PTLEN);
		patn[PTLEN] = '\0';

		printf("----------------offset(%u)--------------------\n", i);

		gettimeofday(&tb, NULL);
		for (j = 0; j < REPEAT; ++j) {
			result = strstr(haystack, patn);
			if (!result) {
				fprintf(stderr, "strstr find failed !\n");
				return EINVAL;
			}
		}
		microsec[0] = calc_microsec(&tb, "strstr");

		gettimeofday(&tb, NULL);
		for (j = 0; j < REPEAT; ++j) {
			result = native_find(haystack, patn, PTLEN);
			if (!result) {
				fprintf(stderr, "strstr find failed !\n");
				return EINVAL;
			}
		}
		microsec[1] = calc_microsec(&tb, "native");

		gettimeofday(&tb, NULL);
		strkmp_init(patn, table_sgs);
		for (j = 0; j < REPEAT; ++j) {
			result = strkmp_find(haystack, patn, table_sgs);
			if (!result) {
				fprintf(stderr, "kmp find failed !\n");
				return EINVAL;
			}
		}
		microsec[2] = calc_microsec(&tb, "kmp");

		gettimeofday(&tb, NULL);
		strbmh_init(patn, PTLEN, table_ebc);
		for (j = 0; j < REPEAT; ++j) {
			result = strbmh_find(haystack, patn, TLEN, PTLEN,
					     table_ebc);
			if (!result) {
				fprintf(stderr, "bmh find failed !\n");
				return EINVAL;
			}
		}
		microsec[3] = calc_microsec(&tb, "bmh");

		gettimeofday(&tb, NULL);
		strbm_init(patn, PTLEN, table_sgs, table_ebc);
		for (j = 0; j < REPEAT; ++j) {
			result = strbm_find(haystack, patn, TLEN, PTLEN,
					    table_sgs, table_ebc);
			if (!result) {
				fprintf(stderr, "bm find failed !\n");
				return EINVAL;
			}
		}
		microsec[4] = calc_microsec(&tb, "bm");

		do_sort(i, microsec, desc, sizeof(desc)/sizeof(desc[0]));
	}

	return 0;
}

int main()
{
	int ret;

	/*
	ret = correct_verify();
	if (ret) {
		return ret;
	}
	printf("string correct_verify success !\n");
	*/

	ret = perf_verify();
	if (ret) {
		return ret;
	}
	printf("string perf_verify success !\n");

	return 0;
}
